package com.butiridan.simularecoada;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

@SuppressWarnings("serial")
public class InputPanel extends JPanel {

	private GridBagConstraints gbc;
	private JTextField tfMinArrivalTime;
	private JTextField tfMaxArrivalTime;
	private JTextField tfMinServiceTime;
	private JTextField tfMaxServiceTime;
	private JTextField tfNrCozi;
	private JComboBox<Integer> cbCurrentDateS;
	private JComboBox<Integer> cbCurrentDateM;
	private JComboBox<Integer> cbCurrentDateH;
	private JComboBox<Integer> cbOpenDateS;
	private JComboBox<Integer> cbOpenDateM;
	private JComboBox<Integer> cbOpenDateH;
	private JComboBox<Integer> cbCloseDateS;
	private JComboBox<Integer> cbCloseDateM;
	private JComboBox<Integer> cbCloseDateH;

	/**
	 * Create the panel.
	 */
	public InputPanel() {

		gbc = new GridBagConstraints();
		this.setLayout(new GridBagLayout());
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.insets = new Insets(5, 5, 5, 5);

		JLabel lblIntervalArrival = new JLabel("Interval sosire clienti:");
		gbc.gridx = 0;
		gbc.gridy = 0;
		this.add(lblIntervalArrival, gbc);
		lblIntervalArrival.setHorizontalAlignment(SwingConstants.CENTER);

		tfMinArrivalTime = new JTextField();
		gbc.gridx = 1;
		gbc.gridy = 0;
		this.add(tfMinArrivalTime, gbc);

		JLabel lblMinus1 = new JLabel("-");
		gbc.gridx = 2;
		gbc.gridy = 0;
		this.add(lblMinus1, gbc);
		lblMinus1.setHorizontalAlignment(SwingConstants.CENTER);

		tfMaxArrivalTime = new JTextField();
		gbc.gridx = 3;
		gbc.gridy = 0;
		this.add(tfMaxArrivalTime, gbc);

		JLabel lblIntervalService = new JLabel("Interval de servire clienti:");
		gbc.gridx = 0;
		gbc.gridy = 1;
		this.add(lblIntervalService, gbc);
		lblIntervalService.setHorizontalAlignment(SwingConstants.CENTER);

		tfMinServiceTime = new JTextField();
		gbc.gridx = 1;
		gbc.gridy = 1;
		this.add(tfMinServiceTime, gbc);

		JLabel lblMinus2 = new JLabel("-");
		gbc.gridx = 2;
		gbc.gridy = 1;
		this.add(lblMinus2, gbc);
		lblMinus2.setHorizontalAlignment(SwingConstants.CENTER);

		tfMaxServiceTime = new JTextField();
		gbc.gridx = 3;
		gbc.gridy = 1;
		this.add(tfMaxServiceTime, gbc);
		
		JLabel lblnrCozi = new JLabel(
				"Numarul de cozi:");
		gbc.gridx = 0;
		gbc.gridy = 2;
		this.add(lblnrCozi, gbc);
		lblnrCozi.setHorizontalAlignment(SwingConstants.CENTER);
		
		tfNrCozi = new JTextField();
		gbc.gridx = 1;
		gbc.gridy = 2;
		this.add(tfNrCozi, gbc);

		JLabel lblOraDeschidere = new JLabel(
				"Ora de deschidere magazin (h:m:s):");
		gbc.gridx = 0;
		gbc.gridy = 3;
		this.add(lblOraDeschidere, gbc);
		lblOraDeschidere.setHorizontalAlignment(SwingConstants.CENTER);

		cbOpenDateH = new JComboBox<Integer>();
		gbc.gridx = 1;
		gbc.gridy = 3;
		this.add(cbOpenDateH, gbc);

		cbOpenDateM = new JComboBox<Integer>();
		gbc.gridx = 2;
		gbc.gridy = 3;
		this.add(cbOpenDateM, gbc);

		cbOpenDateS = new JComboBox<Integer>();
		gbc.gridx = 3;
		gbc.gridy = 3;
		this.add(cbOpenDateS, gbc);
		
		JLabel lblOraInchidere = new JLabel(
				"Ora de inchidere magazin (h:m:s):");
		gbc.gridx = 0;
		gbc.gridy = 4;
		this.add(lblOraInchidere, gbc);
		lblOraInchidere.setHorizontalAlignment(SwingConstants.CENTER);
		
		cbCloseDateH = new JComboBox<Integer>();
		gbc.gridx = 1;
		gbc.gridy = 4;
		this.add(cbCloseDateH, gbc);

		cbCloseDateM = new JComboBox<Integer>();
		gbc.gridx = 2;
		gbc.gridy = 4;
		this.add(cbCloseDateM, gbc);

		cbCloseDateS = new JComboBox<Integer>();
		gbc.gridx = 3;
		gbc.gridy = 4;
		this.add(cbCloseDateS, gbc);
		
		JLabel lblOraCurenta = new JLabel(
				"Ora incepere simulare (h:m:s):");
		gbc.gridx = 0;
		gbc.gridy = 5;
		this.add(lblOraCurenta, gbc);
		lblOraCurenta.setHorizontalAlignment(SwingConstants.CENTER);
		
		cbCurrentDateH = new JComboBox<Integer>();
		gbc.gridx = 1;
		gbc.gridy = 5;
		this.add(cbCurrentDateH, gbc);

		cbCurrentDateM = new JComboBox<Integer>();
		gbc.gridx = 2;
		gbc.gridy = 5;
		this.add(cbCurrentDateM, gbc);

		cbCurrentDateS = new JComboBox<Integer>();
		gbc.gridx = 3;
		gbc.gridy = 5;
		this.add(cbCurrentDateS, gbc);
		
		initJComboBoxH();
		initJComboBoxMS();

	}

	private void initJComboBoxH() {
		for (int i = 0; i < 24; ++i) {
			cbCurrentDateH.addItem(new Integer(i));
			cbOpenDateH.addItem(new Integer(i));
			cbCloseDateH.addItem(new Integer(i));
		}
	}

	private void initJComboBoxMS() {
		for (int i = 0; i < 60; ++i) {
			cbCurrentDateM.addItem(new Integer(i));
			cbCurrentDateS.addItem(new Integer(i));
			cbOpenDateM.addItem(new Integer(i));
			cbOpenDateS.addItem(new Integer(i));
			cbCloseDateM.addItem(new Integer(i));
			cbCloseDateS.addItem(new Integer(i));
		}
	}

	public JTextField getTfMinArrivalTime() {
		return tfMinArrivalTime;
	}

	public void setTfMinArrivalTime(JTextField tfMinArrivalTime) {
		this.tfMinArrivalTime = tfMinArrivalTime;
	}

	public JTextField getTfMaxArrivalTime() {
		return tfMaxArrivalTime;
	}

	public void setTfMaxArrivalTime(JTextField tfMaxArrivalTime) {
		this.tfMaxArrivalTime = tfMaxArrivalTime;
	}

	public JTextField getTfMinServiceTime() {
		return tfMinServiceTime;
	}

	public void setTfMinServiceTime(JTextField tfMinServiceTime) {
		this.tfMinServiceTime = tfMinServiceTime;
	}

	public JTextField getTfMaxServiceTime() {
		return tfMaxServiceTime;
	}

	public void setTfMaxServiceTime(JTextField tfMaxServiceTime) {
		this.tfMaxServiceTime = tfMaxServiceTime;
	}

	public JComboBox<Integer> getCbCurrentDateS() {
		return cbCurrentDateS;
	}

	public void setCbCurrentDateS(JComboBox<Integer> cbCurrentDateS) {
		this.cbCurrentDateS = cbCurrentDateS;
	}

	public JComboBox<Integer> getCbCurrentDateM() {
		return cbCurrentDateM;
	}

	public void setCbCurrentDateM(JComboBox<Integer> cbCurrentDateM) {
		this.cbCurrentDateM = cbCurrentDateM;
	}

	public JComboBox<Integer> getCbCurrentDateH() {
		return cbCurrentDateH;
	}

	public void setCbCurrentDateH(JComboBox<Integer> cbCurrentDateH) {
		this.cbCurrentDateH = cbCurrentDateH;
	}

	public JComboBox<Integer> getCbOpenDateS() {
		return cbOpenDateS;
	}

	public void setCbOpenDateS(JComboBox<Integer> cbOpenDateS) {
		this.cbOpenDateS = cbOpenDateS;
	}

	public JComboBox<Integer> getCbOpenDateM() {
		return cbOpenDateM;
	}

	public void setCbOpenDateM(JComboBox<Integer> cbOpenDateM) {
		this.cbOpenDateM = cbOpenDateM;
	}

	public JComboBox<Integer> getCbOpenDateH() {
		return cbOpenDateH;
	}

	public void setCbOpenDateH(JComboBox<Integer> cbOpenDateH) {
		this.cbOpenDateH = cbOpenDateH;
	}

	public JComboBox<Integer> getCbCloseDateS() {
		return cbCloseDateS;
	}

	public void setCbCloseDateS(JComboBox<Integer> cbCloseDateS) {
		this.cbCloseDateS = cbCloseDateS;
	}

	public JComboBox<Integer> getCbCloseDateM() {
		return cbCloseDateM;
	}

	public void setCbCloseDateM(JComboBox<Integer> cbCloseDateM) {
		this.cbCloseDateM = cbCloseDateM;
	}

	public JComboBox<Integer> getCbCloseDateH() {
		return cbCloseDateH;
	}

	public void setCbCloseDateH(JComboBox<Integer> cbCloseDateH) {
		this.cbCloseDateH = cbCloseDateH;
	}

	public JTextField getTfNrCozi() {
		return tfNrCozi;
	}

	public void setTfNrCozi(JTextField tfNrCozi) {
		this.tfNrCozi = tfNrCozi;
	}
	
	

}
