package com.butiridan.simularecoada;

public class Date {
	private int s, m, h;

	public Date() {
		this.s = 0;
		this.m = 0;
		this.h = 0;
	}

	public Date(Date newDate) {
		this.s = newDate.getS();
		this.m = newDate.getM();
		this.h = newDate.getH();
	}

	public Date(int newH, int newM, int newS) {
		if (newS >= 0 && newS <= 59) {
			this.s = newS;
		} else {
			this.s = 0;
		}
		if (newM >= 0 && newM <= 59) {
			this.m = newM;
		} else {
			this.m = 0;
		}
		if (newH >= 0 && newH <= 23) {
			this.h = newH;
		} else {
			this.h = 0;
		}
	}

	public int getS() {
		return s;
	}

	public void setS(int s) {
		if (s >= 0 && s <= 59) {
			this.s = s;
		} else {
			this.s = 0;
		}
	}

	public int getM() {
		return m;
	}

	public void setM(int m) {
		if (m >= 0 && m <= 59) {
			this.m = m;
		} else {
			this.m = 0;
		}
	}

	public int getH() {
		return h;
	}

	public void setH(int h) {
		if (h >= 0 && h <= 23) {
			this.h = h;
		} else {
			this.h = 0;
		}
	}
	
	public void setDate(Date newDate){
		this.s = newDate.getS();
		this.m = newDate.getM();
		this.h = newDate.getH();
	}
	
	public String stringFormat(){
		return String.format("(%2d:%2d:%2d)", h, m, s);
	}
	
	public int toSec(){
		return this.h * 3600 + this.m * 60 + this.s;
	}
	
	public Date toDate(int x){
		Date aux = new Date();
		aux.setH(x / 3600);
		aux.setM(x / 60);
		aux.setS(x % 60);
		return aux;
	}
	
	public int compar(Date x){
		if(this.h == x.getS()){
			if(this.m == x.getM()){
				if(this.s < x.getS()){
					return -1;
				}else{
					return 1;
				}
			}else if(this.m < x.getM()){
				return -1;
			}else{
				return 1;
			}
		}else if(this.h < x.getS()){
			return -1;
		}else{
			return 1;
		}
	}

}
