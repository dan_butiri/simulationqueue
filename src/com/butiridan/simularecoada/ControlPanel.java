package com.butiridan.simularecoada;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JSlider;
import javax.swing.border.LineBorder;

@SuppressWarnings("serial")
public class ControlPanel extends JPanel {

	private GridBagConstraints gbc;
	private JButton btnSetInput;
	private JButton btnStart;
	private JButton btnHold;
	private JButton btnStop;
	private JSlider sldrSpeedUp;
	private JLabel lblSpeed;
	private JProgressBar pbTimeUp;
	private JLabel lblTime;
	
	/**
	 * Create the panel.
	 */
	public ControlPanel() {
		
		this.setBorder(new LineBorder(Color.GRAY,1));
		this.setLayout(new GridBagLayout());
		this.gbc = new GridBagConstraints();
		this.setPreferredSize(new Dimension(1024, 40));
		
		gbc.insets = new Insets(5, 5, 5, 5);
		gbc.fill = GridBagConstraints.HORIZONTAL;
		
		btnSetInput = new JButton("Set Input");
		gbc.gridx = 1;
		gbc.gridy = 0;
		this.add(btnSetInput, gbc);
		
		btnStart = new JButton(">");
		gbc.gridx = 2;
		gbc.gridy = 0;
		this.add(btnStart, gbc);
		
		btnHold = new JButton("||");
		gbc.gridx = 3;
		gbc.gridy = 0;
		this.add(btnHold, gbc);
		
		btnStop = new JButton("=");
		gbc.gridx = 4;
		gbc.gridy = 0;
		this.add(btnStop, gbc);
		
		sldrSpeedUp = new JSlider();
		gbc.gridx = 5;
		gbc.gridy = 0;
		this.add(sldrSpeedUp, gbc);
		sldrSpeedUp.setPreferredSize(new Dimension(485,20));
		sldrSpeedUp.setMaximum(1000);
		sldrSpeedUp.setMinimum(2);
		
		lblSpeed = new JLabel(sldrSpeedUp.getValue() + " ms");
		gbc.gridx = 6;
		gbc.gridy = 0;
		this.add(lblSpeed, gbc);
		lblSpeed.setPreferredSize(new Dimension(50,20));
		
		pbTimeUp = new JProgressBar();
		gbc.gridx = 7;
		gbc.gridy = 0;
		this.add(pbTimeUp, gbc);
		pbTimeUp.setStringPainted(true);
		
		lblTime = new JLabel(" 0: 0: 0");
		gbc.gridx = 8;
		gbc.gridy = 0;
		this.add(lblTime, gbc);
		lblTime.setPreferredSize(new Dimension(60,20));

	}

	public GridBagConstraints getGbc() {
		return gbc;
	}

	public void setGbc(GridBagConstraints gbc) {
		this.gbc = gbc;
	}

	public JButton getBtnSetInput() {
		return btnSetInput;
	}

	public void setBtnSetInput(JButton btnSetInput) {
		this.btnSetInput = btnSetInput;
	}

	public JButton getBtnStart() {
		return btnStart;
	}

	public void setBtnStart(JButton btnStart) {
		this.btnStart = btnStart;
	}

	public JButton getBtnHold() {
		return btnHold;
	}

	public void setBtnHold(JButton btnHold) {
		this.btnHold = btnHold;
	}

	public JButton getBtnStop() {
		return btnStop;
	}

	public void setBtnStop(JButton btnStop) {
		this.btnStop = btnStop;
	}

	public JSlider getSldrSpeedUp() {
		return sldrSpeedUp;
	}

	public void setSldrSpeedUp(JSlider sldrSpeedUp) {
		this.sldrSpeedUp = sldrSpeedUp;
	}

	public JLabel getLblSpeed() {
		return lblSpeed;
	}

	public void setLblSpeed(JLabel lblSpeed) {
		this.lblSpeed = lblSpeed;
	}

	public JProgressBar getPbTimeUp() {
		return pbTimeUp;
	}

	public void setPbTimeUp(JProgressBar pbTimeUp) {
		this.pbTimeUp = pbTimeUp;
	}

	public JLabel getLblTime() {
		return lblTime;
	}

	public void setLblTime(JLabel lblTime) {
		this.lblTime = lblTime;
	}

	
	
}
