package com.butiridan.simularecoada;

import java.util.ArrayList;
import java.util.Random;

public class CheckPoint extends Thread implements Subject {

	private Shop shop;
	private VirtualTime virtualTime;
	private int minServiceTime, maxServiceTime, serviceTime;
	private Random rn;
	private int nrCasa;
	private EventType eventType;
	private ArrayList<Observer> observers;
	private Customer lastCustomerService;
	private Customer lastCustomerDeparture;
	private static int lastCoada;

	public CheckPoint(Shop newShop, VirtualTime newVirtualTime, int nrCasa) {

		this.shop = newShop;
		this.virtualTime = newVirtualTime;
		this.minServiceTime = 10;
		this.maxServiceTime = 20;
		this.rn = new Random();
		this.serviceTime = rn.nextInt(maxServiceTime - minServiceTime)
				+ minServiceTime;
		this.nrCasa = nrCasa;
		this.eventType = null;
		this.observers = new ArrayList<Observer>();

	}

	public void run() {
		while (shop.isOpen()) {
			try {
				if(this.isAlive()) Thread.sleep(1);
			} catch (Exception e) {
			}
			if (!shop.getQueue(nrCasa).isEmpty()) {	
				shop.getQueue(nrCasa).getFirst()
						.setArrivalTime(virtualTime.getDate());
				shop.getQueue(nrCasa).getFirst().setServiceTime(serviceTime);
				
				lastCustomerService = shop.getQueue(nrCasa).getFirst();
				lastCoada = shop.getQueue(nrCasa).getFirst().getNoQueue();
				
				eventType = EventType.service;
				notifyObserver();
				
				try {
					Thread.sleep(serviceTime * virtualTime.getSpeedUp());
				
				
				shop.getQueue(nrCasa).getFirst()
						.setDepartureTime(virtualTime.getDate());
				serviceTime = rn.nextInt(maxServiceTime - minServiceTime)
						+ minServiceTime;
				/*System.out.println(shop
						.getQueue(nrCasa)
						.getFirst()
						.getStringFormat(
								shop.getQueue(nrCasa).getFirst()
										.getArrivalInLineTime())
						+ shop.getQueue(nrCasa)
								.getFirst()
								.getStringFormat(
										shop.getQueue(nrCasa).getFirst()
												.getArrivalTime())
						+ shop.getQueue(nrCasa).getFirst().getServiceTime()
						+ shop.getQueue(nrCasa)
								.getFirst()
								.getStringFormat(
										shop.getQueue(nrCasa).getFirst()
												.getDepartureTime())
						+ shop.getQueue(nrCasa).getFirst().getNoQueue()
						+ " "
						+ shop.getQueue(nrCasa).getFirst().getName());*/
				
				lastCustomerDeparture = shop.getQueue(nrCasa).getFirst();
				shop.setNrClientiCoada(shop.getNrClientiCoada() - 1);
				if(shop.getQueue(nrCasa).size() > 0) {shop.getQueue(nrCasa).removeFirst();
				
				eventType = EventType.departure;
				notifyObserver();
				}
				} catch (Exception e) {
				}
			}
		}
	}

	public int getMinServiceTime() {
		return minServiceTime;
	}

	public void setMinServiceTime(int minServiceTime) {
		this.minServiceTime = minServiceTime;
	}

	public int getMaxServiceTime() {
		return maxServiceTime;
	}

	public void setMaxServiceTime(int maxServiceTime) {
		this.maxServiceTime = maxServiceTime;
	}

	public Customer getLastCustomerService() {
		return lastCustomerService;
	}

	public void setLastCustomerService(Customer lastCustomerService) {
		this.lastCustomerService = lastCustomerService;
	}

	public Customer getLastCustomerDeparture() {
		return lastCustomerDeparture;
	}

	public void setLastCustomerDeparture(Customer lastCustomerDeparture) {
		this.lastCustomerDeparture = lastCustomerDeparture;
	}
	
	public static int getLastCoada() {
		return lastCoada;
	}

	public static void setLastCoada(int lastCoada) {
		CheckPoint.lastCoada = lastCoada;
	}

	public void addObserver(Observer newObserver) {
		observers.add(newObserver);

	}

	public void removeObserver(Observer removeObserver) {
		int observerIndex = observers.indexOf(removeObserver);

		observers.remove(observerIndex);

	}

	public void notifyObserver() {
		for (Observer observer : observers) {
			observer.update(eventType);
		}

	}

}
