package com.butiridan.simularecoada;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class ControlClass implements Observer {

	private MainFrame mainFrame;
	private VirtualTime virtualTime;
	private Shop shop;
	private CustomerGenerator customerGenerator;
	private Thread t1;
	private Subject subjectTime;
	private Subject subjectEvent;
	private Subject subjectCheck;
	private Date date, dateSel, dateOpen, dateClose;
	private ControlPanel controlPanel;
	private JTextArea log;
	private InputPanel inputPanel;
	private int nrCozi;
	private int mimSosireClient, maxSosireClient, minService, maxService;
	private boolean hold = false;
	private Date timpAsteptareCoada;
	private Date timpAsteptareCasa;
	private int nrTotalClienti;
	private Date timpMinimAsteptareCoada;
	private Date timpMaximAsteptareCoada;
	private Date timpMinimAsteptareCasa;
	private Date timpMaximAsteptareCasa;

	public ControlClass() {

		nrCozi = 1;
		mimSosireClient = 10;
		maxSosireClient = 20;
		minService = 10;
		maxService = 20;
		this.date = new Date();
		this.dateSel = new Date();
		this.dateOpen = new Date(8, 0, 0);
		this.dateClose = new Date(20, 0, 0);

		this.mainFrame = new MainFrame();
		this.virtualTime = new VirtualTime();
		this.shop = new Shop(dateOpen, dateClose, virtualTime, nrCozi);
		this.customerGenerator = new CustomerGenerator(shop, mimSosireClient,
				maxSosireClient, virtualTime);
		this.inputPanel = new InputPanel();
		this.t1 = new Thread(virtualTime);

		this.subjectTime = virtualTime;
		this.subjectTime.addObserver(this);
		this.subjectEvent = shop;
		this.subjectEvent.addObserver(this);
		if (shop.isOpen()) {
			for (int i = 0; i < nrCozi; ++i) {
				this.subjectCheck = shop.getCheckPoint(i);
				this.subjectCheck.addObserver(this);
			}
		}

		timpAsteptareCoada = new Date();
		timpAsteptareCasa = new Date();
		nrTotalClienti = 0;
		timpMinimAsteptareCoada = new Date(23, 59, 59);
		timpMaximAsteptareCoada = new Date();
		timpMinimAsteptareCasa = new Date(23, 59, 59);
		timpMaximAsteptareCasa = new Date();

		controlPanel = mainFrame.getContentPane().getControlPanel();
		log = mainFrame.getContentPane().getLogPanel().getTaLog();

		virtualTime.setSpeedUp(controlPanel.getSldrSpeedUp().getValue());

		jobSlider();
		jobBtnHold();
		jobBtnStart();
		jobBtnStop();
		jobBtnSetInput();

	}

	private void jobSlider() {
		controlPanel.getSldrSpeedUp().addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg) {
				virtualTime
						.setSpeedUp(controlPanel.getSldrSpeedUp().getValue());
				controlPanel.getLblSpeed().setText(
						"" + controlPanel.getSldrSpeedUp().getValue() + " ms");
			}
		});
	}

	private void jobBtnHold() {
		controlPanel.getBtnHold().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg) {
				virtualTime.setStop(true);
				hold = true;
			}
		});
	}

	private void jobBtnStart() {
		controlPanel.getBtnStart().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg) {
				if (hold) {
					hold = false;
					t1 = new Thread(virtualTime);
					virtualTime.setStop(false);
					t1.start();
				}
				if (t1.getState() == Thread.State.NEW) {
					virtualTime.setH(dateSel.getH());
					virtualTime.setM(dateSel.getM());
					virtualTime.setS(dateSel.getS());

					t1.start();
				}
				if (t1.getState() == Thread.State.TERMINATED) {
					t1 = new Thread(virtualTime);

					virtualTime.setH(dateSel.getH());
					virtualTime.setM(dateSel.getM());
					virtualTime.setS(dateSel.getS());
					virtualTime.setStop(false);
					log.setText("");

					t1.start();
				}
			}
		});
	}

	private void jobBtnStop() {
		controlPanel.getBtnStop().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg) {

				virtualTime.setStop(true);
			}
		});
	}

	private void jobBtnSetInput() {
		controlPanel.getBtnSetInput().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg) {
				String aux;

				inputPanel.getTfNrCozi().setText("" + nrCozi);
				inputPanel.getTfMinArrivalTime().setText("" + mimSosireClient);
				inputPanel.getTfMaxArrivalTime().setText("" + maxSosireClient);
				inputPanel.getTfMinServiceTime().setText("" + minService);
				inputPanel.getTfMaxServiceTime().setText("" + maxService);

				JOptionPane.showMessageDialog(null, inputPanel, "Custom Field",
						1);
				try {
					aux = inputPanel.getTfNrCozi().getText();
					nrCozi = Integer.parseInt(aux);
					shop.setNoQueue(nrCozi);
					addCoada();

					aux = inputPanel.getTfMinArrivalTime().getText();
					mimSosireClient = Integer.parseInt(aux);

					aux = inputPanel.getTfMaxArrivalTime().getText();
					maxSosireClient = Integer.parseInt(aux);

					if (mimSosireClient >= maxSosireClient) {
						maxSosireClient += mimSosireClient;
					}
					customerGenerator.setMinArrivalTime(mimSosireClient);
					customerGenerator.setMaxArrivalTime(maxSosireClient);

					aux = inputPanel.getTfMinServiceTime().getText();
					minService = Integer.parseInt(aux);

					aux = inputPanel.getTfMaxServiceTime().getText();
					maxService = Integer.parseInt(aux);

					if (minService >= maxService) {
						maxService += minService;
					}
					if (shop.isOpen()) {
						for (int i = 0; i < nrCozi; ++i) {
							shop.getCheckPoint(i).setMinServiceTime(minService);
							shop.getCheckPoint(i).setMaxServiceTime(maxService);
						}
					}
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, "Date incomplete...");
				}

				aux = inputPanel.getCbCurrentDateH().getSelectedItem()
						.toString();
				dateSel.setH(Integer.parseInt(aux));
				aux = inputPanel.getCbCurrentDateM().getSelectedItem()
						.toString();
				dateSel.setM(Integer.parseInt(aux));
				aux = inputPanel.getCbCurrentDateS().getSelectedItem()
						.toString();
				dateSel.setS(Integer.parseInt(aux));

				aux = inputPanel.getCbOpenDateH().getSelectedItem().toString();
				dateOpen.setH(Integer.parseInt(aux));
				aux = inputPanel.getCbOpenDateM().getSelectedItem().toString();
				dateOpen.setM(Integer.parseInt(aux));
				aux = inputPanel.getCbOpenDateS().getSelectedItem().toString();
				dateOpen.setS(Integer.parseInt(aux));

				aux = inputPanel.getCbCloseDateH().getSelectedItem().toString();
				dateClose.setH(Integer.parseInt(aux));
				aux = inputPanel.getCbCloseDateM().getSelectedItem().toString();
				dateClose.setM(Integer.parseInt(aux));
				aux = inputPanel.getCbCloseDateS().getSelectedItem().toString();
				dateClose.setS(Integer.parseInt(aux));

			}
		});
	}

	public void update(Object o) {

		if (o.equals(virtualTime)) {
			date.setDate(((VirtualTime) o).getDate());

			controlPanel.getLblTime().setText(date.stringFormat());

			if (shop.isOpen()) {
				controlPanel.getPbTimeUp().setMinimum(0);
				controlPanel.getPbTimeUp().setValue(
						calcSec(date) - calcSec(dateOpen));
				controlPanel.getPbTimeUp().setMaximum(
						calcSec(dateClose) - calcSec(dateOpen));
				controlPanel.getPbTimeUp().setString("Deschis!");
			}/*
			 * else{ controlPanel.getPbTimeUp().setMinimum(0);
			 * controlPanel.getPbTimeUp().setValue(calcSec(date) -
			 * calcSec(dateClose));
			 * controlPanel.getPbTimeUp().setMaximum(calcSec(dateOpen) -
			 * calcSec(dateClose));
			 * controlPanel.getPbTimeUp().setString("Inchis!"); }
			 */
		}
		if ((o == EventType.service)) {
			@SuppressWarnings("static-access")
			Customer auxCustomer = shop.getCheckPoint(
					shop.getCheckPoint(0).getLastCoada())
					.getLastCustomerService();

			timpAsteptareCoada = addDate(timpAsteptareCoada,
					auxCustomer.getAstaptaCoadaTimp());
			if(timpMaximAsteptareCoada.compar(auxCustomer.getAstaptaCoadaTimp()) == -1){
				timpMaximAsteptareCoada.setDate(auxCustomer.getAstaptaCoadaTimp());
			}
			if(timpMinimAsteptareCoada.compar(auxCustomer.getAstaptaCoadaTimp()) == 1){
				timpMinimAsteptareCoada.setDate(auxCustomer.getAstaptaCoadaTimp());
			}

			log.append("\n"
					+ date.stringFormat()// auxCustomer.getStringFormat(auxCustomer.getArrivalTime())
					+ " Clientul " + auxCustomer.getName()
					+ " este servit la casa " + (auxCustomer.getNoQueue() + 1)
					+ ". Timpul de asteptare la coada: "
					+ auxCustomer.getTimpDeAsteptareCoada());

			log.setCaretPosition(log.getText().length() - 1);
		}
		if ((o == EventType.departure)) {
			if (shop.isOpen()) {
				@SuppressWarnings("static-access")
				Customer auxCustomer = shop.getCheckPoint(
						shop.getCheckPoint(0).getLastCoada())
						.getLastCustomerDeparture();

				if (auxCustomer != null) {
					timpAsteptareCasa = addDate(timpAsteptareCasa,
							auxCustomer.getAstaptaCasaTimp());
					if(timpMaximAsteptareCasa.compar(auxCustomer.getAstaptaCasaTimp()) == -1){
						timpMaximAsteptareCasa.setDate(auxCustomer.getAstaptaCasaTimp());
					}
					if(timpMinimAsteptareCasa.compar(auxCustomer.getAstaptaCasaTimp()) == 1){
						timpMinimAsteptareCasa.setDate(auxCustomer.getAstaptaCasaTimp());
					}
					
					log.append("\n"
							+ date.stringFormat()
							+ " Clientul "
							+ auxCustomer.getName()
							+ " a parasit magazinul! Timp de asteptare la casa: "
							+ auxCustomer.getServiceTime());

					log.setCaretPosition(log.getText().length() - 1);

					addOm();
				} else {
					for (int i = 0; i < nrCozi; ++i) {
						mainFrame.getContentPane().getListaCozi()
								.getCoadaPanel(i).setNrClienti(0);
					}
				}
			}
		}
		if ((o == EventType.arrival_in_line)) {
			Customer auxCustomer = shop.getLastCustomerInLine();

			++nrTotalClienti;

			log.append("\n" + date.stringFormat()
					// auxCustomer.getStringFormat(auxCustomer.getArrivalInLineTime())
					+ " Clientul " + auxCustomer.getName()
					+ " s-a pus la coada " + (auxCustomer.getNoQueue() + 1));

			log.setCaretPosition(log.getText().length() - 1);

			addOm();
		}
		if ((o == EventType.open_shop)) {
			log.setText("\n" + virtualTime.getStringFormat()
					+ " Magasinul s-a deschis!" + "\n");

			log.setCaretPosition(log.getText().length() - 1);

			for (int i = 0; i < nrCozi; ++i) {
				this.subjectCheck = shop.getCheckPoint(i);
				this.subjectCheck.addObserver(this);
			}
		}
		if ((o == EventType.close_shop)) {
			nrTotalClienti -= shop.getNrClientiCoada();
			log.append("\n" + virtualTime.getStringFormat()
					+ " Magasinul s-a inchis!" + "\n");
			log.append("\n\n\n\n\n");
			log.append("\nNumarul total de clienti: " + nrTotalClienti);
			log.append("\nTimpul total de asteptare la casa: "
					+ timpAsteptareCasa.stringFormat());
			log.append("\nTimpul total de asteptare la coada: "
					+ timpAsteptareCoada.stringFormat());
			if(nrTotalClienti != 0){
			log.append("\nTimpul minim de asteptare la casa: "
					+ timpMinimAsteptareCasa.stringFormat());
			}else{
				log.append("\nTimpul maxim de asteptare la casa: "
						+ timpMaximAsteptareCasa.stringFormat());
			}
			log.append("\nTimpul maxim de asteptare la casa: "
					+ timpMaximAsteptareCasa.stringFormat());
			if(nrTotalClienti != 0){
			log.append("\nTimpul minim de asteptare la coada: "
					+ timpMinimAsteptareCoada.stringFormat());
			}else{
				log.append("\nTimpul minim de asteptare la coada: "
						+ timpMaximAsteptareCoada.stringFormat());
			}
			log.append("\nTimpul maxim de asteptare la coada: "
					+ timpMaximAsteptareCoada.stringFormat());
			if (nrTotalClienti != 0) {
				log.append("\nTimpul mediu de asteptare la casa: "
						+ timpAsteptareCasa.toDate(
								timpAsteptareCasa.toSec() / nrTotalClienti)
								.stringFormat());
				log.append("\nTimpul mediu de asteptare la coada: "
						+ timpAsteptareCoada.toDate(
								timpAsteptareCoada.toSec() / nrTotalClienti)
								.stringFormat());
			} else {
				log.append("\nTimpul mediu de asteptare la casa: 0");
				log.append("\nTimpul mediu de asteptare la coada: 0");
			}

			log.append("\n\n\n");

			log.setCaretPosition(log.getText().length() - 1);
		}

	}

	private void addOm() {
		for (int i = 0; i < nrCozi; ++i) {
			mainFrame.getContentPane().getListaCozi().getCoadaPanel(i)
					.setNrClienti(shop.getQueue(i).size());
		}
	}

	private void addCoada() {
		mainFrame.getContentPane().getListaCozi().setNrCozi(nrCozi);
	}

	private int calcSec(Date d) {
		return (d.getH() * 360 + d.getM() * 60 + d.getS());
	}

	private Date addDate(Date x, Date y) {
		Date z = new Date();
		int auxS, auxM, auxH;
		auxS = x.getS() + y.getS();
		auxM = x.getM() + y.getM();
		auxH = x.getH() + y.getH();
		z.setS(auxS % 60);
		z.setM((auxM + (auxS / 60)) % 60);
		z.setH(auxH + auxM / 60);

		return z;
	}

}
