package com.butiridan.simularecoada;

import javax.swing.JFrame;

@SuppressWarnings("serial")
public class MainFrame extends JFrame {

	private MainPanel contentPane;

	/**
	 * Create the frame.
	 */
	public MainFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Simulare!");
		setResizable(false);
		setVisible(true);
		contentPane = new MainPanel();
		setContentPane(contentPane);
		pack();
	}

	public MainPanel getContentPane() {
		return contentPane;
	}

}
