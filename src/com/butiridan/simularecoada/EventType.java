package com.butiridan.simularecoada;

public enum EventType {
	arrival_in_line, arrival, service, departure, open_shop, close_shop;
}
