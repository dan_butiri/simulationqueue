package com.butiridan.simularecoada;

public interface Observer {
	public void update(Object o);
}
