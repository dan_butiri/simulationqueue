package com.butiridan.simularecoada;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

@SuppressWarnings("serial")
public class ListaCoziPanel extends JPanel {

	private ArrayList<CoadaPanel> coadaPanel;
	private JPanel casa;
	private JScrollPane scroll;
	private int nrCozi;

	/**
	 * Create the panel.
	 */
	public ListaCoziPanel() {

		this.setBorder(new LineBorder(Color.GRAY,1));
		this.coadaPanel = new ArrayList<CoadaPanel>();
		this.casa = new JPanel();
		this.scroll = new JScrollPane(casa);
		this.nrCozi = 1;

		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scroll.setPreferredSize(new Dimension(1020, 400));
		this.add(scroll);

		deseneazaCozi(nrCozi);

	}

	public void deseneazaCozi(int newNrCozi) {
		coadaPanel.removeAll(coadaPanel);
		casa.removeAll();
		scroll.revalidate();
		
		casa.setLayout(new GridLayout(newNrCozi, 1));
		
		for (int i = 0; i < newNrCozi; ++i) {
			coadaPanel.add(new CoadaPanel("Casa: " + (i+1)));
			casa.add(coadaPanel.get(i));
		}
	}
	
	public int getNrCozi() {
		return nrCozi;
		
	}
	
	public void setNrCozi(int nrCozi) {
		this.nrCozi = nrCozi;
		deseneazaCozi(this.nrCozi);
	}

	public CoadaPanel getCoadaPanel(int index) {
		return coadaPanel.get(index);
	}
	
}
