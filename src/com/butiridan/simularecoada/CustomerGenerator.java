package com.butiridan.simularecoada;

import java.util.Random;

public class CustomerGenerator implements Observer {

	private Random rn;
	private int minArrivalTime, maxArrivalTime, ArrivalTime, count = 0;
	private Shop shop;
	private static int countCustomer = 0;
	private Subject subject;

	public CustomerGenerator(Shop newShop, int newMinArrivalTime,
			int newMaxArrivalTime, VirtualTime newVirtualTime) {
		this.rn = new Random();
		this.minArrivalTime = newMinArrivalTime;
		this.maxArrivalTime = newMaxArrivalTime;
		this.ArrivalTime = rn.nextInt(maxArrivalTime - minArrivalTime)
				+ minArrivalTime;
		this.shop = newShop;
		this.subject = newVirtualTime;
		this.subject.addObserver(this);
	}

	public void update(Object o) {
		genereaza();

	}

	private void genereaza() {

		if (shop.isOpen()) {
			if (count == ArrivalTime) {
				shop.addCustomer(new Customer("" + countCustomer));
				++countCustomer;
				ArrivalTime = rn.nextInt(maxArrivalTime - minArrivalTime)
						+ minArrivalTime;
				count = 0;
			} else {
				++count;
			}
		} else {
			countCustomer = 0;
			count = 0;
		}
	}

	public int getMinArrivalTime() {
		return minArrivalTime;
	}

	public void setMinArrivalTime(int minArrivalTime) {
		this.minArrivalTime = minArrivalTime;
	}

	public int getMaxArrivalTime() {
		return maxArrivalTime;
	}

	public void setMaxArrivalTime(int maxArrivalTime) {
		this.maxArrivalTime = maxArrivalTime;
	}
	
	
}
