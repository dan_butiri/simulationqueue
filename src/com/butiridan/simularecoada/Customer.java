package com.butiridan.simularecoada;

public class Customer {

	private String name;
	private Date arrivalTime, departureTime, arrivalInLineTime;
	private int serviceTime;
	private int noQueue;

	public Customer(String name) {
		this.name = name;
		this.arrivalTime = new Date(0, 0, 0);
		this.serviceTime = 0;
		this.arrivalInLineTime = new Date(0, 0, 0);
		this.noQueue = 0;
	}

	public Customer(String name, Date arrivalTime, Date arrivalInLineTime,
			int serviceTime, int newNoQueue) {
		this.name = name;
		this.arrivalTime = arrivalTime;
		this.serviceTime = serviceTime;
		this.arrivalInLineTime = arrivalInLineTime;
		this.noQueue = newNoQueue;
	}
	
	public int getTimpDeAsteptareCoada(){
		int timp;
		timp = this.arrivalTime.getH()*360 - this.arrivalInLineTime.getH()*360
				+ this.arrivalTime.getM()*60 - this.arrivalInLineTime.getM()*60
				+ this.arrivalTime.getS() - this.arrivalInLineTime.getS();
		return timp;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(Date arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public int getServiceTime() {
		return serviceTime;
	}

	public void setServiceTime(int serviceTime) {
		this.serviceTime = serviceTime;
	}

	public Date getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(Date departureTime) {
		this.departureTime = departureTime;
	}

	public Date getArrivalInLineTime() {
		return arrivalInLineTime;
	}

	public void setArrivalInLineTime(Date arrivalInLineTime) {
		this.arrivalInLineTime = arrivalInLineTime;
	}

	public int getNoQueue() {
		return noQueue;
	}

	public void setNoQueue(int noQueue) {
		this.noQueue = noQueue;
	}

	public String getStringFormat(Date d) {
		return String.format("(%2d:%2d:%2d)", d.getH(), d.getM(), d.getS());
	}
	
	public Date getAstaptaCoadaTimp(){
		Date d = new Date();
		d.setH(this.arrivalTime.getH() - this.arrivalInLineTime.getH());
		d.setM(this.arrivalTime.getM() - this.arrivalInLineTime.getM());
		d.setS(this.arrivalTime.getS() - this.arrivalInLineTime.getS());
		return d;
	}
	
	public Date getAstaptaCasaTimp(){
		Date d = new Date();
		d.setH(this.departureTime.getH() - this.arrivalTime.getH());
		d.setM(this.departureTime.getM() - this.arrivalTime.getM());
		d.setS(this.departureTime.getS() - this.arrivalTime.getS());
		return d;
	}

}
