package com.butiridan.simularecoada;

import java.util.ArrayList;

public class Shop implements Observer, Subject {

	private Date openTime, closeTime, currentTime;
	private Customer lastCustomerInLine;
	private VirtualTime virtualTime;
	private Subject subject;
	private ArrayList<Observer> observers;
	private EventType eventType;
	private int auxIsOpen;
	private ArrayList<Queue<Customer>> queue;
	private ArrayList<CheckPoint> checkPoint;
	private int noQueue;
	private int nrClientiCoada;

	public Shop(Date newOpenTime, Date newCloseTime,
			VirtualTime newVirtualTime, int newNoQueue) {
		this.openTime = newOpenTime;
		this.closeTime = newCloseTime;
		this.currentTime = new Date();
		this.virtualTime = newVirtualTime;
		this.subject = newVirtualTime;
		this.subject.addObserver(this);
		this.observers = new ArrayList<Observer>();
		this.eventType = null;
		this.auxIsOpen = 0;
		this.nrClientiCoada = 0;
		this.queue = new ArrayList<Queue<Customer>>();
		this.checkPoint = new ArrayList<CheckPoint>();
		this.noQueue = newNoQueue;
		
	}

	public void addCustomer(Customer newCustomer) {
		pickQueue(newCustomer);
		++nrClientiCoada;
	}

	private void open() {
		this.queue = new ArrayList<Queue<Customer>>();
		this.checkPoint = new ArrayList<CheckPoint>();
		
		for (int i = 0; i < noQueue; ++i){
			queue.add(new Queue<Customer>());
			checkPoint.add(new CheckPoint(this, virtualTime,i));
			checkPoint.get(i).start();
		}	
		
		eventType = EventType.open_shop;
		notifyObserver();
		
	}

	private void close() {
		
		eventType = EventType.close_shop;
		notifyObserver();
		
	}

	private void pickQueue(Customer newCustomer) {

		int minQueue = minQueue();

		newCustomer.setArrivalInLineTime(virtualTime.getDate());
		newCustomer.setNoQueue(minQueue);
		
		lastCustomerInLine = newCustomer;
		
		queue.get(minQueue).addAtEnd(newCustomer);
		
		eventType = EventType.arrival_in_line;
		notifyObserver();
	}
	
	private int minQueue() {
		int min = queue.get(0).size();
		int minIndex = 0;

		for (int i = 0; i < queue.size(); ++i) {
			if (min > queue.get(i).size()) {
				min = queue.get(i).size();
				minIndex = i;
			}
		}

		return minIndex;
	}

	public boolean isOpen() {
		boolean okOpen = false, okClose = false;
		if (openTime.getH() == currentTime.getH()) {
			if (openTime.getM() == currentTime.getM()) {
				if (openTime.getS() <= currentTime.getS()) {
					okOpen = true;
				}
			} else if (openTime.getM() < currentTime.getM()) {
				okOpen = true;
			}
		} else if (openTime.getH() < currentTime.getH()) {
			okOpen = true;
		}
		if (closeTime.getH() == currentTime.getH()) {
			if (closeTime.getM() == currentTime.getM()) {
				if (closeTime.getS() >= currentTime.getS()) {
					okClose = true;
				}
			} else if (closeTime.getM() > currentTime.getM()) {
				okClose = true;
			}
		} else if (closeTime.getH() > currentTime.getH()) {
			okClose = true;
		}

		if (okOpen && okClose) {
			return true;
		} else {
			return false;
		}
	}

	public void update(Object o) {
		currentTime.setS(((VirtualTime) o).getS());
		currentTime.setM(((VirtualTime) o).getM());
		currentTime.setH(((VirtualTime) o).getH());
		if (isOpen()) {
			if (auxIsOpen == 0) {
				open();
				++auxIsOpen;
			}
		} else {
			if (auxIsOpen == 1) {
				close();
				auxIsOpen = 0;
			}
		}
	}

	public void addObserver(Observer newObserver) {

		observers.add(newObserver);
		
	}

	public void removeObserver(Observer removeObserver) {
		int observerIndex = observers.indexOf(removeObserver);
		
		observers.remove(observerIndex);
		
	}

	public synchronized void notifyObserver() {
		
		for(Observer observer : observers){
			observer.update(eventType);
		}
		
	}

	public Customer getLastCustomerInLine() {
		return lastCustomerInLine;
	}

	public void setLastCustomerInLine(Customer lastCustomerInLine) {
		this.lastCustomerInLine = lastCustomerInLine;
	}

	public Queue<Customer> getQueue(int i) {
		return queue.get(i);
	}

	public int getNoQueue() {
		return noQueue;
	}

	public void setNoQueue(int noQueue) {
		/*for (int i = 0; i < noQueue; ++i){
			queue.add(new Queue<Customer>());
			checkPoint.add(new CheckPoint(this, virtualTime,i));
			checkPoint.get(i).start();
		}*/
		this.noQueue = noQueue;
	}

	public CheckPoint getCheckPoint(int i) {
		return checkPoint.get(i);
	}

	public void setCheckPoint(ArrayList<CheckPoint> checkPoint) {
		this.checkPoint = checkPoint;
	}

	public int getNrClientiCoada() {
		return nrClientiCoada;
	}

	public void setNrClientiCoada(int nrClientiCoada) {
		this.nrClientiCoada = nrClientiCoada;
	}
	
	
}
