package com.butiridan.simularecoada;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.LinkedList;
import java.util.Random;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class CoadaPanel extends JPanel {

	private int x1 = 10, y1 = 90, x2 = 900, y2 = 90;
	private String name;
	private int nrClienti;
	private LinkedList<Color> color;

	public CoadaPanel(String name) {

		this.setPreferredSize(new Dimension(1000, 100));
		this.name = name;
		this.nrClienti = 0;
		this.color = new LinkedList<Color>();

	}

	public void paint(Graphics g) {
		g.setColor(Color.DARK_GRAY);
		g.fillRect(0, 0, 1020, 400);
		g.setColor(Color.BLACK);
		g.drawLine(x1, y1, x2, y2);
		g.drawLine(x1 + 1, y1 + 1, x2 + 1, y2 + 1);
		g.drawLine(x1 + 2, y1 + 2, x2 + 2, y2 + 2);
		g.draw3DRect(x1 + x2 + 10, 10, 80, 80, true);
		deseneaaVanzator(g);
		g.setColor(Color.BLACK);
		g.drawString(name, 925, 25);
		for (int i = nrClienti - 1; i >= 0; --i) {
			deseneazaOm(i, g);
		}
	}

	public void deseneazaOm(int x, Graphics g) {
		int i = x;
		x *= 35;
		g.setColor(color.get(i));
		g.fillOval(x2 - x - 40, 5, 45, 45);
		g.setColor(new Color(242, 178, 103));
		g.fillOval(x2 - x - 15, 15, 20, 25);
		g.setColor(color.get(i));
		g.fillRect(x2 - x - 40, 50, 45, 40);
		g.setColor(new Color(242, 178, 103));
		g.fillOval(x2 - x - 15, 60, 30, 7);
		g.setColor(Color.BLUE);
		g.fillOval(x2 - x - 5, 20, 5, 5);
		g.fillOval(x2 - x - 10, 20, 5, 5);
		g.setColor(Color.BLACK);
		g.fillRect(x2 - x - 10, 70, 8, 4);
		g.fillRect(x2 - x - 10, 80, 8, 4);
	}

	public void deseneaaVanzator(Graphics g) {

		g.setColor(Color.BLACK);
		g.fillRect(x2 + 20, 65, 35, 25);
		g.fillRect(x2 + 20, 50, 10, 25);

		g.setColor(Color.BLUE);
		g.fillOval(x2 + 64, 20, 35, 35);
		g.setColor(new Color(242, 178, 103));
		g.fillOval(x2 + 67, 28, 15, 20);
		g.setColor(Color.BLUE);
		g.fillRect(x2 + 64, 55, 35, 35);
		g.setColor(new Color(242, 178, 103));
		g.fillOval(x2 + 48, 60, 30, 7);
		g.setColor(Color.BLACK);
		g.fillOval(x2 + 70, 30, 4, 4);
		g.fillOval(x2 + 74, 30, 4, 4);
	}

	public void setNrClienti(int nrClienti) {
		if (nrClienti >= this.nrClienti) {

			for (int i = 0; i < nrClienti - this.nrClienti; ++i) {
				Random rn = new Random();
				float rC = rn.nextFloat();
				float gC = rn.nextFloat();
				float bC = rn.nextFloat();
				color.addLast(new Color(rC, gC, bC));
			}

		} else {
			color.removeFirst();
		}

		this.nrClienti = nrClienti;
		this.repaint();
	}

}
