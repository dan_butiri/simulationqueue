package com.butiridan.simularecoada;

import java.util.LinkedList;

public class Queue<T> {
	
	private LinkedList<T> list;
	
	public Queue(){
		list = new LinkedList<T>();
	}
	
    public void addAtEnd( T element) {
        list.add(element);
    }

    public T removeFirst() {
        return list.removeFirst();
    }
    
    public T removeLast() {
        return list.removeLast();
    }
    
    public T getFirst(){
    	return list.getFirst();
    }
    
    public T getLast(){
    	return list.getLast();
    }
    
    public int size(){
    	return list.size();
    }
    
    public boolean isEmpty(){
    	return list.isEmpty();
    }
	
}
