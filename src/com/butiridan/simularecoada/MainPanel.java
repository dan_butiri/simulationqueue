package com.butiridan.simularecoada;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class MainPanel extends JPanel {

	private GridBagConstraints gbc = new GridBagConstraints();
	private LogPanel logPanel = new LogPanel();
	private ControlPanel controlPanel = new ControlPanel();
	private ListaCoziPanel listaCozi;
	
	/**
	 * Create the panel.
	 */
	public MainPanel() {
		
		this.setLayout(new GridBagLayout());
		this.listaCozi = new ListaCoziPanel();
		
		gbc.fill = GridBagConstraints.HORIZONTAL;
		
		gbc.insets = new Insets(10, 10, 0, 10);
		gbc.gridx = 0;
		gbc.gridy = 0;
		this.add(listaCozi, gbc);
		
		gbc.insets = new Insets(0, 10, 0, 10);
		gbc.gridx = 0;
		gbc.gridy = 1;
		this.add(controlPanel, gbc);
		
		gbc.insets = new Insets(0, 10, 10, 10);
		gbc.gridx = 0;
		gbc.gridy = 2;
		this.add(logPanel, gbc);
		
	}

	public LogPanel getLogPanel() {
		return logPanel;
	}

	public void setLogPanel(LogPanel logPanel) {
		this.logPanel = logPanel;
	}

	public ControlPanel getControlPanel() {
		return controlPanel;
	}

	public void setControlPanel(ControlPanel controlPanel) {
		this.controlPanel = controlPanel;
	}

	public ListaCoziPanel getListaCozi() {
		return listaCozi;
	}

	public void setListaCozi(ListaCoziPanel listaCozi) {
		this.listaCozi = listaCozi;
	}
	
	

}
