package com.butiridan.simularecoada;

import java.util.ArrayList;

public class VirtualTime implements Runnable, Subject {

	private int speedUp, s, m, h;
	private ArrayList<Observer> observers;
	private boolean stop;

	public VirtualTime() {
		speedUp = 1000;
		s = 0;
		m = 0;
		h = 0;
		observers = new ArrayList<Observer>();
		stop = false;
	}

	public VirtualTime(int newS, int newM, int newH, int newSpeedUp) {
		this.speedUp = newSpeedUp;
		if (newS >= 0 && newS <= 59) {
			this.s = newS;
		} else {
			this.s = 0;
		}
		if (newM >= 0 && newM <= 59) {
			this.m = newM;
		} else {
			this.m = 0;
		}
		if (newH >= 0 && newH <= 23) {
			this.h = newH;
		} else {
			this.h = 0;
		}
		observers = new ArrayList<Observer>();
		stop = false;
	}

	public void run() {

		while (!stop) {
			try {
				Thread.sleep(speedUp);
				notifyObserver();
				addSec();
			} catch (Exception e) {
			}
		}

	}

	public void addObserver(Observer newObserver) {
		observers.add(newObserver);

	}

	public void removeObserver(Observer removeObserver) {
		int observerIndex = observers.indexOf(removeObserver);

		observers.remove(observerIndex);

	}

	public synchronized void notifyObserver() {

		for (Observer observer : observers) {
			observer.update(this);
		}

	}

	private void addSec() {
		if (s < 59) {
			++s;
		} else if (m < 59) {
			s = 0;
			++m;
		} else if (h < 23) {
			s = 0;
			m = 0;
			++h;
		} else {
			s = 0;
			m = 0;
			h = 0;
		}
	}

	public int getSpeedUp() {
		return speedUp;
	}

	public void setSpeedUp(int speedUp) {
		this.speedUp = speedUp;
	}

	public int getS() {
		return s;
	}

	public void setS(int s) {
		if (s >= 0 && s <= 59) {
			this.s = s;
		} else {
			this.s = 0;
		}
	}

	public int getM() {
		return m;
	}

	public void setM(int m) {
		if (m >= 0 && m <= 59) {
			this.m = m;
		} else {
			this.m = 0;
		}
	}

	public int getH() {
		return h;
	}

	public void setH(int h) {
		if (h >= 0 && h <= 23) {
			this.h = h;
		} else {
			this.h = 0;
		}
	}

	public String getStringFormat() {
		return String.format("(%2d:%2d:%2d)", h, m, s);
	}

	public Date getDate() {
		Date date = new Date();

		date.setS(s);
		date.setM(m);
		date.setH(h);

		return date;
	}

	public boolean isStop() {
		return stop;
	}

	public void setStop(boolean stop) {
		this.stop = stop;
	}
	
	

}
