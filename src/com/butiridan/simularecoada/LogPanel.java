package com.butiridan.simularecoada;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

@SuppressWarnings("serial")
public class LogPanel extends JPanel {

	private JTextArea taLog;
	private JScrollPane scroll;
	/**
	 * Create the panel.
	 */
	public LogPanel() {
		this.setBorder(new LineBorder(Color.GRAY,1));
		
		taLog = new JTextArea();
		scroll = new JScrollPane(taLog);
		
		this.add(scroll);
		scroll.setPreferredSize(new Dimension(1024, 200));
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		
		taLog.setFont(new Font("Courier New", Font.PLAIN, 13));
		taLog.setWrapStyleWord(true);
		taLog.setLineWrap(true);
		taLog.setEditable(false);
		taLog.setBackground(Color.BLACK);
		taLog.setForeground(Color.GREEN);
		taLog.setBorder(new LineBorder(Color.BLACK,10));
		
	}

	public JTextArea getTaLog() {
		return taLog;
	}
	public void setTaLog(JTextArea taLog) {
		this.taLog = taLog;
	}
}
